#!/usr/bin/env pwsh
#--------------------------------------------------------------
#
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        helper_function_1        # Calling helper_function_1
#
#        helper_function_2        # Calling helper_function_2
#    }
#
#    #---------------------------------------------------------
#    # Helper Function 1
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Helper Function 2
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something else
#    }
#
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
#---------------------------------------------------------------------------------
param(
      [string]$HOME_USER        = "HOME_USER",
      [string]$INSTALL_FOLDER   = "INSTALL_FOLDER",
      [string]$ARDUINO_VERSION  = "Arduino_VERSION",
      [string]$BOARD_VENDOR     = "BOARD_VENDOR",
      [string]$BOARD            = "BOARD",
      [string]$CPU              = "CPU",
      [string]$PRODUCT          = "PRODUCT",
      [string]$SKETCH           = "SKETCH",
      [string]$SKETCH_VER       = "SKETCH_VER",
	  [string]$ARCHITECTURE     = "ARCHITECTURE",
      [string]$PROTOCOL         = "PROTOCOL",
      [string]$PROBE            = "PROBE"
      ) ;


#Read-Host -Prompt "Pausing:  Press any key to continue"

$USERNAME        = "$env:USER"                                 # $env:VAR_NAME="VALUE"
$KERNEL          = "lin"
$BASE_PATH       = "${HOME_USER}/${INSTALL_FOLDER}"
$PROJECTS_PATH   = "$BASE_PATH/Projects"
$PROJECT_NAME    = "${CPU}-${SKETCH}"
$BUILD_PATH      = "${PROJECTS_PATH}/${PROJECT_NAME}/build"
$OPENOCD_PATH    = "${BASE_PATH}/openocd/bin"
$OPENOCD_CONFIG  = "board/${BOARD_VENDOR}/${KERNEL}/${PROBE}/${PRODUCT}-${SKETCH}-cmake-${PROTOCOL}.cfg"
#Read-Host -Prompt "Pausing:  Press any key to continue"



#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)  Entering                main"

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)  Calling                 echo_args"
    echo_args                                                                        # Calling echo_args
#Read-Host -Prompt "Pausing:  Press any key to continue"

    echo "Line $(CurrentLine)   Calling                Launch_jtagit_prog()"
    Launch_jtagit_prog

}

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
#$PREFS_PATH = "W:/"
$MyVariable = 1

    Write-Host "Line $(CurrentLine)   Entering               echo_args"
    Write-Host "Line $(CurrentLine)   Arguments              as follows";
    Write-Host "Line $(CurrentLine)   HOME_USER            = ${HOME_USER}";
    Write-Host "Line $(CurrentLine)   INSTALL_FOLDER       = ${INSTALL_FOLDER}";
    Write-Host "Line $(CurrentLine)   Board Vendor         = ${BOARD_VENDOR}";
    Write-Host "Line $(CurrentLine)  Board Name           = ${BOARD}";
    Write-Host "Line $(CurrentLine)  CPU                  = ${CPU}";
    Write-Host "Line $(CurrentLine)  PRODUCT              = ${PRODUCT}";
    Write-Host "Line $(CurrentLine)  Sketch               = ${SKETCH}";
    Write-Host "Line $(CurrentLine)  Architecture         = ${ARCHITECTURE}";
    Write-Host "Line $(CurrentLine)  PROTOCOL             = ${PROTOCOL}";
    Write-Host "Line $(CurrentLine)  Debug Probe          = ${PROBE}";
    Write-Host "Line $(CurrentLine)  End Of Arguments               ";
    Write-Host "Line $(CurrentLine)  BASE_PATH            = ${BASE_PATH}";
    Write-Host "Line $(CurrentLine)  PROJECTS_PATH        = ${PROJECTS_PATH}";
    Write-Host "Line $(CurrentLine)  PROJECT_NAME         = ${PROJECT_NAME}";                
    Write-Host "Line $(CurrentLine)  BUILD_PATH           = ${BUILD_PATH}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_PATH         = ${OPENOCD_PATH}";            
    Write-Host "Line $(CurrentLine)  OPENOCD_PATH         = ${OPENOCD_CONFIG}";            
    Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH}";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args"
}

#---------------------------------------------------------
# Launch launcf_jtagit_prog
#---------------------------------------------------------
function launch_jtagit_prog
{
    Write-Host "Line $(CurrentLine)  Entering               Launch_jtagit_prog()"
    Write-Host "Line $(CurrentLine)  Executing             'cd ${BUILD_PATH}' "
    cd ${BUILD_PATH}
    Write-Host "Line $(CurrentLine)  PWD                  = $PWD "
    Write-Host "Line $(CurrentLine)  Protocol             = ${PROTOCOL} "
	Write-Host "Line $(CurrentLine)  Executing             '& ${OPENOCD_PATH}/openocd -f ${OPENOCD_CONFIG}' "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & ${OPENOCD_PATH}/openocd -f ${OPENOCD_CONFIG}

#Read-Host -Prompt "Pausing:  Press any key to continue"

    cd ~
    Write-Host "Line $(CurrentLine)  PWD                  = $PWD "
    
    Write-Host "Line $(CurrentLine)  Leaving                launch_jtagit_prog"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
echo "Line $(CurrentLine)  Calling                main()"
main
